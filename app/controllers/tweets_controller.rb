class TweetsController < ApplicationController


  def index
    @tweets = Tweet.all
  end

  def new
  end

  def create
    current_user.tweet.create(tweet_params)
    Tweet.create(title: tweet_params[:title], content: tweet_params[:content], user_id: current_user.id)
    redirect_to root_path
  end

  def destroy
    tweet = Tweet.find(params[:id])
    if tweet.user_id == current_user.id
      tweet.destroy
      redirect_to root_path
    end
  end

  def edit
    @tweet = Tweet.find(params[:id])
  end

  def update
    redirect_to root_url
  end

  def show
    @tweet = Tweet.find(params[:id])
  end

  private

    def tweet_params
      params.permit(:title, :content)
    end
end
